-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20-Nov-2019 às 01:52
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `site_niggas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens_contatos`
--

CREATE TABLE IF NOT EXISTS `mensagens_contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `mensagem` varchar(220) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `mensagens_contatos`
--

INSERT INTO `mensagens_contatos` (`id`, `nome`, `email`, `mensagem`, `created`, `modified`) VALUES
(1, 'Cristovao', 'admin@gmail.com', '123', '2019-11-19 17:44:20', '0000-00-00 00:00:00'),
(2, 'Cristovao', 'admin@gmail.com', '123', '2019-11-19 19:51:44', '0000-00-00 00:00:00'),
(3, 'Cristovao', 'vinesinho23@gmail.com', '123', '2019-11-19 19:53:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id_produto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `quantidade` varchar(10) NOT NULL,
  `preco` decimal(10,2) NOT NULL,
  `imagem` text NOT NULL,
  `detalhes` varchar(300) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `imagem2` text NOT NULL,
  `imagem3` text NOT NULL,
  `imagem4` text NOT NULL,
  `imagem5` text NOT NULL,
  PRIMARY KEY (`id_produto`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id_produto`, `nome`, `quantidade`, `preco`, `imagem`, `detalhes`, `modelo`, `imagem2`, `imagem3`, `imagem4`, `imagem5`) VALUES
(1, 'camisa branca', '10', '50.00', 'camisabranca.jpg', 'Camiseta 100% poliester de cor branca com estampa do triangulo iluminati em homenagem ao professor Ramon Venson.\r\nEla e perfeita para uso em casual com uma aparencia moderna e elegante.', 'Niggas Illuminati Branca', 'camisaN2mini1.jpg', 'camisaN2mini2.jpg', 'camisaN2mini3.jpg', 'camisaN2mini4.jpg'),
(2, 'camisa preta', '5', '50.00', 'camisapreta.jpg', 'Camiseta 100% poliester de cor pretacom estampa do triangulo niggas em homenagem ao nosso editor batatinha sagaz.\r\nEla e perfeita para uso em casual com uma aparencia moderna e elegante.', 'Niggas Negron Black', 'camisaN3mini1.jpg', 'camisaN3mini2.jpg', 'camisaN3mini3.jpg', 'camisaN3mini4.jpg'),
(3, 'camisa rosa', '6', '50.00', 'camisarosa.jpg', 'Camiseta 100% poliester de cor rosa com estampa do niggas supreme farpando a supreme que esta atualmente com 50% menos lucro que a niggasco.\r\nEla e perfeita para uso em casual com uma aparencia moderna e elegante.', 'Niggas Supreme', 'camisaN4mini1.jpg', 'camisaN4mini2.jpg', 'camisaN4mini3.jpg', 'camisaN4mini4.jpg'),
(4, 'camisa branca 2', '20', '50.00', 'camisaN1mini1.jpg', 'Camiseta 100% poliester de cor branca com estampa do niggasxx linda de bonita.\r\nEla e perfeita para uso em casual com uma aparencia moderna e elegante.', 'Niggas Basica', 'camisaN1mini1.jpg', 'camisaN1mini2.jpg', 'camisaN1mini3.jpg', 'camisaN1mini4.jpg'),
(5, 'camisa cinza', '34', '50.00', 'camisaN5mini1.jpg', 'Camiseta 100% poliester de cor cinza com estampa do Niggas NG linda de bonita.\r\nEla e perfeita para uso em casual com uma aparencia moderna e elegante.', 'Niggas NG', 'camisaN5mini1.jpg', 'camisaN5mini2.jpg', 'camisaN5mini3.jpg', 'camisaN5mini4.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `senha` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nome`, `telefone`, `email`, `senha`) VALUES
(1, 'asdsadasd', '31234124', 'adsadas@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(2, 'Pedro Nicolau', '49991656617', 'backespedro2016@gmail.com', '202cb962ac59075b964b07152d234b70'),
(3, 'Maria Eduarda', '991137291', 'mbackesjovenal@gmail.com', '202cb962ac59075b964b07152d234b70'),
(4, 'Joao Pedro', '9911111111', 'joao@gmail.com', '202cb962ac59075b964b07152d234b70'),
(5, 'Pedro Nicolau', '3456345', 'kadu_cardoso00@hotmail.com', '202cb962ac59075b964b07152d234b70'),
(6, 'Vinicius Bolox', '231129412', 'vinibolox@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(7, 'Maria Eduardaa', '123434234', 'kadu_cardoso002@hotmail.com', '202cb962ac59075b964b07152d234b70'),
(8, 'Pedro Nicolaue', '23142342', 'joao111@gmail.com', '202cb962ac59075b964b07152d234b70'),
(9, 'Vinicius dos Reis Correa', 'a', 'admin12345@gmail.com', '202cb962ac59075b964b07152d234b70');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
